import { useContext, useEffect} from 'react';

import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';



export default function Logout(){

	// consume the UserContext object and destructure it to access the state and unsetUset function from context provider. 
	const { unsetUser, setUser } = useContext (UserContext);

		// localStorage.clear()
	// Clear the localStorage of the user's information
	unsetUser();

	useEffect(() => {
		setUser({id: null});
	})
		
	return (
		<Navigate to="/login" />
	)
};